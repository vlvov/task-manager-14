package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
