package ru.t1.vlvov.tm.comparator;

import ru.t1.vlvov.tm.api.model.IHasName;
import ru.t1.vlvov.tm.api.model.IHasStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHasStatus> {

    INSTANCE;

    public int compare(final IHasStatus o1, final IHasStatus o2){
        if(o1 == null || o2 == null) return 0;
        if(o1.getStatus() == null || o2.getStatus() == null) return 0;
        return o1.getStatus().getDisplayName().compareTo(o2.getStatus().getDisplayName());
    }
}
