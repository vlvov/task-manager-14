package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.IProjectTaskService;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.remove(task);
        projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }
}
